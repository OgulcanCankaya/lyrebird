FROM openjdk:8-jdk-alpine
EXPOSE 9090
COPY target/casedstudy-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]


