resource "aws_ecs_cluster" "lyrebird-ecs-cluster" {
  name = "ecs-cluster-for-lyrebird"
}

resource "aws_ecs_service" "lyrebird-ecs-service-two" {
  name            = "lyrebird-app"
  cluster         = aws_ecs_cluster.lyrebird-ecs-cluster.id
  task_definition = aws_ecs_task_definition.lyrebird-ecs-task-definition.arn
  launch_type     = "FARGATE"
  network_configuration {
    subnets          = ["subnet-4ee66a02"]
    assign_public_ip = true
  }
  desired_count = 1
}

resource "aws_iam_role" "ecs_simulator_task_role" {
  name = "ecs_simulator_task_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_ecs_task_definition" "lyrebird-ecs-task-definition" {
  family                   = "ecs-task-definition-lyrebird"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  memory                   = "1024"
  cpu                      = "512"
  execution_role_arn       = "${aws_iam_role.ecs_simulator_task_role.arn}"
  container_definitions    = <<EOF
[
  {
    "name": "lyrebird-container",
    "image": "582198882075.dkr.ecr.eu-central-1.amazonaws.com/lyrebird:latest",
    "memory": 1024,
    "cpu": 512,
    "essential": true,
    "entryPoint": ["/"],
    "portMappings": [
      {
        "containerPort": 9090,
        "hostPort": 9090
      }
    ]
  }
]
EOF
}