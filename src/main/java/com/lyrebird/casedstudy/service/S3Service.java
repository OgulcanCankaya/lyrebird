package com.lyrebird.casedstudy.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient;
import com.amazonaws.services.cloudfront.model.CreateInvalidationRequest;
import com.amazonaws.services.cloudfront.model.CreateInvalidationResult;
import com.amazonaws.services.cloudfront.model.InvalidationBatch;
import com.amazonaws.services.cloudfront.model.Paths;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.lyrebird.casedstudy.config.S3Config;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@Slf4j
public class S3Service {
    @Value("${application.bucket.name}")
    private String bucketName;

    @Value("${cloud.aws.distribution.id}")
    private String distributionId;

    @Autowired
    private AmazonS3 s3Client;

    public String uploadFile(MultipartFile file) {
        File fileObj = convertMultiPartFileToFile(file);
        String fileName = file.getOriginalFilename();
        System.out.println(fileName);
        System.out.println(bucketName);
        //If an object is already there, this is an update operation!
        if (s3Client.doesObjectExist(bucketName, file.getOriginalFilename())) {

            AmazonCloudFrontClient client = new AmazonCloudFrontClient();
            Paths invalidation_paths = new Paths().withItems(fileName).withQuantity(1);

            InvalidationBatch invalidation_batch = new InvalidationBatch(invalidation_paths, fileName + System.currentTimeMillis());
            CreateInvalidationRequest invalidation = new CreateInvalidationRequest(distributionId, invalidation_batch);
            // TODO: There is a sdk mismatch on this! possibly caused by the versions only.
            CreateInvalidationResult ret = client.createInvalidation(invalidation);
            s3Client.putObject(new PutObjectRequest(bucketName, fileName, fileObj));
            fileObj.delete();

            return "File updated : " + fileName;
        }

        s3Client.putObject(new PutObjectRequest(bucketName, fileName, fileObj));
        fileObj.delete();
        return "File uploaded : " + fileName;
    }

    /**
     * Downloads a file from s3
     * @param fileName Object to be downloaded
     * @return
     */
    public byte[] downloadFile(String fileName) {
        S3Object s3Object = s3Client.getObject(bucketName, fileName);
        S3ObjectInputStream inputStream = s3Object.getObjectContent();
        try {
            byte[] content = IOUtils.toByteArray(inputStream);
            return content;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Gets the object from the distribution
     * @param fileName object name/path on the s3 bucket
     * @return byte array
     */
    public byte[] downloadFileFromCloudFront(String fileName) {
        try {

            URL url = new URL("https://do11nr5rp0ua5.cloudfront.net/"+fileName);
            InputStream inputStream = url.openStream();
            byte[] content = IOUtils.toByteArray(inputStream);
            return content;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Deletes the object from s3.
     * @param fileName Can also be referred as objectKey
     * @return Status string
     */
    public String deleteFile(String fileName) {
        s3Client.deleteObject(bucketName, fileName);
        return fileName + " removed ...";
    }

    /**
     *
     * @param file A multipart file received from the request
     * @return A single File containing the merged object
     */
    private File convertMultiPartFileToFile(MultipartFile file) {
        File convertedFile = new File(file.getOriginalFilename());
        try (FileOutputStream fos = new FileOutputStream(convertedFile)) {
            fos.write(file.getBytes());
        } catch (IOException e) {
            log.error("Error converting multipartFile to file", e);
        }
        return convertedFile;
    }

}
