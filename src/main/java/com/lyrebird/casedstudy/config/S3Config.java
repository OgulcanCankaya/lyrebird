package com.lyrebird.casedstudy.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class S3Config  {
    @Value("${cloud.aws.credentials.access-key}")
    private String accessKey;

    @Value("${cloud.aws.credentials.secret-key}")
    private String accessSecret;
    @Value("${cloud.aws.region.static}")
    private String region;

    /**
     * Uses the AccessKey and the Secret Access Key obtained from the application.yml and creates the client.
     * @return AmazonS3 Client for s3 operations
     */
    @Bean
    public AmazonS3 s3Client() {
        //INFO: This could be obtained from SM or a Dynamo Table. But this was quick, even though it was dirty.
        System.out.println(accessKey);
        System.out.println(accessSecret);
        System.out.println(region);
        AWSCredentials credentials = new BasicAWSCredentials(accessKey, accessSecret);
        return AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(region).build();
    }

}
