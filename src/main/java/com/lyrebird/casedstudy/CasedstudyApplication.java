package com.lyrebird.casedstudy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class CasedstudyApplication {
	/**
	 *For documentation please visit http://localhost:9090/v2/api-docs
	 * @param args Generic
	 */
	public static void main(String[] args) {
		SpringApplication.run(CasedstudyApplication.class, args);
	}

}
